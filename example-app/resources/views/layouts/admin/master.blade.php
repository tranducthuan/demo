<!DOCTYPE html>
<html lang="en">

@include('layouts.admin.head');

<body class="g-sidenav-show  bg-gray-200">

    @include('layouts.admin.aside')

    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->
    @include('layouts.admin.navbar')

        <!-- End Navbar -->
        <div class="container-fluid py-4">

            @yield('content')

            @include('layouts.admin.footer')

        </div>
    </main>
    <!--   Core JS Files   -->

    @include('layouts.admin.script')

</body>

</html>
