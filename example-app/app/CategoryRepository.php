<?php

namespace App;

use Illuminate\Support\Collection;
use Pros\CodeBase\Repositories\BaseRepository;
use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    protected $category;
    public function __construct(Category $category)
    {
        parent::__construct();
        return $this->category = $category;
    }

    public function getAllCategory($paginate = 5) {
        return $this->orderBy('id', 'DESC')->paginate($paginate);
    }

    public function addCategory($request) {
        return $this->create($request);
    }

    public function getCategoryById($id) {
        return $this->find($id);
    }

    public function deleteCategoryById($id)
    {
        return $this->destroy($id);
    }

    public function updateCategory($request, $id)
    {
        return $this->where('id', $id)->update($request);
    }
}
