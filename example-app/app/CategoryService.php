<?php

namespace App;

class CategoryService
{
    protected $categoryRepository;
    public function __construct(CategoryRepository $categoryRepository)
    {
        return $this->categoryRepository = $categoryRepository;
    }

    public function getAllCategory($per = 5) {
        return $this->categoryRepository->getAllCategory($per);
    }

    public function addCategory($request) {
        return $this->categoryRepository->addCategory($request);
    }

    public function getCategoryById($id)
    {
        return $this->categoryRepository->getCategoryById($id);
    }

    public function deleteCategory($id)
    {
        return $this->categoryRepository->deleteCategoryById($id);
    }

    public function updateCategoryById($request, $id)
    {
        return $this->categoryRepository->updateCategory($request, $id);
    }
}
