<?php

namespace App;

use Illuminate\Support\Collection;
use Pros\CodeBase\Repositories\BaseRepository;
use App\Models\Product;

class ProductRepository extends BaseRepository
{
    protected $product;

    public function __construct(Product $product)
    {
        parent::__construct();
        return $this->product = $product;
    }

    public function getAllProduct($page = 10)
    {
        return $this->orderBy('id', 'DESC')->paginate($page);
    }

    public function getProductById($id)
    {
        return $this->find($id);
    }

    public function updateProduct($params, $id)
    {
        return $this->where('id', $id)->update($params);
    }

    public function addProduct($params)
    {
        return $this->create($params);
    }

    public function deletePro($id)
    {
        return $this->destroy($id);
    }
}
