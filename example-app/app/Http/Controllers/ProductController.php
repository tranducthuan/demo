<?php

namespace App\Http\Controllers;

use App\CategoryService;
use App\Http\Requests\Request\ProductRequest;
use App\ProductService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Pros\CodeBase\Traits\ResponseTemplateTrait;

class ProductController extends BaseController
{
    use ResponseTemplateTrait;

    protected ProductService $productService;
    protected CategoryService $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
        return $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = $this->productService->getAll();

        $categoryName = $this->categoryService->getCategoryById($data['category_id']);

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductRequest $request)
    {
        if (isset($request->image)) {
            $imageName = $request->file('image')->getClientOriginalName() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
        } else {
            $imageName = 'NULL';
        }

        $arr = array(
            'name' => $request->name,
            'category_id' => $request->category_id,
            'price' => $request->price,
            'quality' => $request->quality,
            'content' => $request->content,
            'image' => $imageName,
            'status' => $request->status
        );

        $data = $this->productService->createProduct($arr);
        return $this->jsonSuccess($data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = $this->productService->getProductById($id);

        return $this->jsonSuccess($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductRequest $request, $id)
    {
        $image = $this->productService->getProductById($id);
        if (isset($request->image)) {
            $imageName = $request->file('image')->getClientOriginalName() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
        } else {
            $imageName = $image['image'];
        }
        $arr = array(
            'name' => $request->name,
            'category_id' => $request->category_id,
            'price' => $request->price,
            'quality' => $request->quality,
            'content' => $request->content,
            'image' => $imageName,
            'status' => $request->status
        );

        $data = $this->productService->updateProduct($arr, $id);

        return $this->jsonSuccess($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $statusProduct = $this->productService->getProductById($id);
        $statusCategory = $this->categoryService->getCategoryById($statusProduct['category_id']);

        if ($statusProduct['status'] == 1 || $statusCategory['status'] == 1) {
            die('a');
        } else {
            $data = $this->productService->deleteProduct($id);

            return $this->jsonSuccess($data);
        }
    }
}
