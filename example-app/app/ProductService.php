<?php

namespace App;
use App\ProductRepository;
use App\Models\Product;

class ProductService
{
    protected $productRepository;
    public function __construct(ProductRepository $productRepository)
    {
        return $this->productRepository = $productRepository;
    }

    public function getAll($per = 5) {
        return $this->productRepository->getAllProduct($per);
    }

    public function getProductById($id)
    {
        return $this->productRepository->getProductById($id);
    }

    public function updateProduct($params, $id)
    {
        return $this->productRepository->updateProduct($params, $id);
    }

    public function createProduct($params)
    {
        return $this->productRepository->addProduct($params);
    }

    public function deleteProduct($id)
    {
        return $this->productRepository->deletePro($id);
    }
}
